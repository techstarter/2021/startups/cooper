# 2. Embedded solution

###### Embedded Programming List 

1. ***Temperature monitoring (alerts when temp is too high or too low depending on user settings)***
2. ***Water/Feed Level alert (alerts when low)***
3. ***Air Quality Control (monitors humidity, ammonia, and CO2)***
4. ***Nesting Box Egg detector (detects when an egg is laid in a box)***
5. Hen laying tracker (tracks which hen laid an egg)
6. Chicken Tracking (alerts when chickens get outside a perimeter such as into a garden or any other area)
7. ***Automatic Door (opens/closes on timer or by light)***
8. Automatic Heating (turns on a "safe" heater when temp drops below a certain point)
9. ***Automatic Cooling (fan kicks on when temp gets above a certain point)***
10. ***Automatic Lighting (controls coop lighting/provides more light when daylight lessens for optimal laying)***
11. Auto Water Defroster (turns on to prevent water from freezing in low temps)
12. ***Video Camera Monitoring***
13. Alerts when preditor is in the area maybe by sound 
14. ***Solar Powered?***

## 2.1 Objectives

<Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken>


## 2.3 Steps taken

![alt text](img/1.png)

#### Components used 

1. Esp32 Wroom 32 
2. Arduino Level Sensor 
3. Dht 11 Sensor (Temperature and Humidity Sensor)
4. Ldr (Light Dependent Resistor)
5. SSD1306  display(0x3c (Oled)
6. Leds for alerts 
7. Breadboard 
8. Jumper Wires 
9. Power Supply 

## 2.4 Testing & Problems

#### Combined Embedded Cooper Code here
###### <a href="https://drive.google.com/file/d/1rX57NZPu2qARgX_rzthJlueAEa9g1Vnl/view?usp=sharing" download> Click to Download Code</a>

```
//Oled Stuff
#include <Wire.h>
#include "SSD1306.h" 
SSD1306  display(0x3c, 21, 22);

//SERVO 
#include <ESP32Servo.h>

Servo myservo;  // create servo object to control a servo
// 16 servo objects can be created on the ESP32

int pos = 0;    // variable to store the servo position
// Recommended PWM GPIO pins on the ESP32 include 2,4,12-19,21-23,25-27,32-33 
int servoPin = 5;

//LDR 
//cosntants for the pins where sensors are plugged into.
const int ldrPin = 4;
const int lampPin = 17;
const int ldrThreshold = 500;

//Set up some global variables for the light level an initial value.
int lightInit;  // initial value
int lightVal;   // light reading

//NOTE ESP32 VALUES GO UP TO 4093
//Water Level 
/* Water Level */
int lowerThreshold = 1000;
int upperThreshold = 2000;

// Sensor pins
#define sensorPower 27
//#define waterPin 25

// Value for storing water level
int val = 0;

// Declare pins to which LEDs are connected
int redLED = 23;
int yellowLED = 19;
int greenLED = 18;

int level = 0;  // holds the value
int waterPin = 25; // sensor pin used

//DHT 11 
#include <Adafruit_Sensor.h>
#include <DHT.h>
#define DHTPIN 27     // Digital pin connected to the DHT sensor
int TempTresh=40;
int HumTresh=40;
// Uncomment the type of sensor in use:
#define DHTTYPE    DHT11     // DHT 11
//#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE    DHT21     // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  pinMode(sensorPower, OUTPUT);
  digitalWrite(sensorPower, LOW);
  
  // Set LED pins as an OUTPUT
  pinMode(redLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
  pinMode(greenLED, OUTPUT);

  // Initially turn off all LEDs
  digitalWrite(redLED, LOW);
  digitalWrite(yellowLED, LOW);
  digitalWrite(greenLED, LOW);

  //Ldr Servo 
  pinMode(lampPin, OUTPUT);
  lightInit = analogRead(ldrPin);
  //we will take a single reading from the light sensor and store it in the lightCal        //variable. This will give us a prelinary value to       compare against in the loop
  // Servo pin
  ESP32PWM::allocateTimer(0);
  ESP32PWM::allocateTimer(1);
  ESP32PWM::allocateTimer(2);
  ESP32PWM::allocateTimer(3);
  myservo.setPeriodHertz(50);    // standard 50 hz servo
  myservo.attach(servoPin, 1000, 2000);

  //DHT11
  dht.begin();

  //Oled Stuff
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
}



void WaterLevel() {
  //int level = analogRead(sensorPin);
  level = analogRead(waterPin);
  if (level == 0) {
    
    Serial.println("Water Level: Empty");
    Serial.println(level);
    digitalWrite(redLED, LOW);
    digitalWrite(yellowLED, LOW);
    digitalWrite(greenLED, LOW);
  }
  else if (level > 0 && level <= lowerThreshold) {
    Serial.println("Water Level: Low");
    Serial.println(level);
    digitalWrite(redLED, HIGH);
    digitalWrite(yellowLED, LOW);
    digitalWrite(greenLED, LOW);
  }
  else if (level > lowerThreshold && level <= upperThreshold) {
    Serial.println("Water Level: Medium");
    Serial.println(level);
    digitalWrite(redLED, LOW);
    digitalWrite(yellowLED, HIGH);
    digitalWrite(greenLED, LOW);
  }
  else if (level > upperThreshold) {
    Serial.println("Water Level: High");
    Serial.println(level);
    digitalWrite(redLED, LOW);
    digitalWrite(yellowLED, LOW);
    digitalWrite(greenLED, HIGH);
  }
  delay(1000);

  //This is a function used to get the reading
}

//int readSensor() {
//  digitalWrite(sensorPower, HIGH);
//  delay(10);
//  val = analogRead(waterPin);
//  digitalWrite(sensorPower, LOW);
//  return val;
//}

void LightLevel()
{

  lightVal = analogRead(ldrPin); // read the current light levels
  Serial.println(lightVal);
  //if lightVal is less than our initial reading withing a threshold then it is dark.
  if(lightVal  < ldrThreshold)
  {
      digitalWrite (lampPin, HIGH); // turn on light
      Serial.println("Light On");
      myservo.write(0);
      delay(500);
      myservo.write(180);
      Serial.println("Door Open");
      delay(2000);
      myservo.write(0);
      Serial.println("Door Closed");
  }

  //otherwise, it is bright
  else
  {
    digitalWrite (lampPin, LOW); // turn off light
    Serial.println("Light Off");
    myservo.write(0);
    Serial.println("Door Closed");
  }

}


void Dht11Sensor() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F(" Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("C "));
  Serial.print(f);
  Serial.print(F("F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("C "));
  Serial.print(hif);
  Serial.println(F("F"));

  // Logig dhtll here

  if (t>=TempTresh & HumTresh) {
     // digitalWrite(AlertaTempBaixa, LOW);
     // digitalWrite(AlertaTempAlta, HIGH);
      Serial.println("Too Humid and Hot");
    }
    else if (h<=TempTresh & HumTresh){
      //digitalWrite(AlertaTempBaixa, HIGH);
      //digitalWrite(AlertaTempAlta, LOW);
      Serial.println("Good Temp Good Humidity");
    }
    else {
     // digitalWrite(AlertaTempBaixa, LOW);
     // digitalWrite(AlertaTempAlta, LOW);
     Serial.println("Below");
    }
}

void loop() {  
  WaterLevel();
  LightLevel();
  Dht11Sensor();
}

```

### ESP32 Async Web Server – Control Outputs with Arduino IDE (ESPAsyncWebServer library)

1. In the first scenario, you toggle the button to turn GPIO 2 on. When that happens, the browser makes an HTTP GET request on the /update?output=2&state=1 URL. Based on that URL, the ESP changes the state of GPIO 2 to 1 (HIGH) and turns the LED on.

2. In the second example, you toggle the button to turn GPIO 2 off. When that happens, the browser makes an HTTP GET request on the /update?output=2&state=0 URL. Based on that URL, we change the state of GPIO 2 to 0 (LOW) and turn the LED off.

###### <a href="https://drive.google.com/file/d/1ytguA7JowszHX2oITpfNOFLjF9Ikf6BQ/view?usp=sharing" download>Click to Download Code </a>

![alt text](img/2.png)

```
/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-async-web-server-espasyncwebserver-library/
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

// Import required libraries
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

// Replace with your network credentials
const char* ssid = "Virus";
const char* password = "RedEyeJedi44";

const char* PARAM_INPUT_1 = "output";
const char* PARAM_INPUT_2 = "state";

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>ESP Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    p {font-size: 3.0rem;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 120px; height: 68px} 
    .switch input {display: none}
    .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
    .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
    input:checked+.slider {background-color: #b30000}
    input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
  </style>
</head>
<body>
  <h2>ESP Web Server</h2>
  %BUTTONPLACEHOLDER%
<script>function toggleCheckbox(element) {
  var xhr = new XMLHttpRequest();
  if(element.checked){ xhr.open("GET", "/update?output="+element.id+"&state=1", true); }
  else { xhr.open("GET", "/update?output="+element.id+"&state=0", true); }
  xhr.send();
}
</script>
</body>
</html>
)rawliteral";

// Replaces placeholder with button section in your web page
String processor(const String& var){
  //Serial.println(var);
  if(var == "BUTTONPLACEHOLDER"){
    String buttons = "";
    buttons += "<h4>Output - GPIO 18</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"18\" " + outputState(18) + "><span class=\"slider\"></span></label>";
    buttons += "<h4>Output - GPIO 19</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"19\" " + outputState(19) + "><span class=\"slider\"></span></label>";
    buttons += "<h4>Output - GPIO 23</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"23\" " + outputState(23) + "><span class=\"slider\"></span></label>";
    return buttons;
  }
  return String();
}

String outputState(int output){
  if(digitalRead(output)){
    return "checked";
  }
  else {
    return "";
  }
}

void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);

  pinMode(18, OUTPUT);
  digitalWrite(18, LOW);
  pinMode(19, OUTPUT);
  digitalWrite(19, LOW);
  pinMode(23, OUTPUT);
  digitalWrite(23, LOW);
  
  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
  server.on("/update", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage1;
    String inputMessage2;
    // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
    if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2)) {
      inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
      inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
      digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
    }
    else {
      inputMessage1 = "No message sent";
      inputMessage2 = "No message sent";
    }
    Serial.print("GPIO: ");
    Serial.print(inputMessage1);
    Serial.print(" - Set to: ");
    Serial.println(inputMessage2);
    request->send(200, "text/plain", "OK");
  });

  // Start server
  server.begin();
}

void loop() {

}

```

### ESP32 Remote Control with WebSocket

<iframe width="560" height="315" src="https://www.youtube.com/embed/8rJ_s-nArKU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

###### <a href="https://drive.google.com/file/d/1GfePiXJS_VREPjnB_V69SfDH8W-S9OHd/view?usp=sharing" download>Click to Download Code </a>

https://m1cr0lab-esp32.github.io/remote-control-with-websocket/web-ui-design/

1. favicon.ico is a small, iconic image that represents the ESP32 website.
2. index.html contains the structural description of the user interface.
3. index.css is the associated style sheet that describes the graphical layout of the interface.
4. index.js is currently an empty file, but will integrate later the Javascript code that will run on the client browser to interact with the user and the ESP32.
note: I put the index.js inside the index.html 

##### index.html

```
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="author" content="Stéphane Calderoni">
  <meta name="description" content="ESP32 Remote Control with WebSocket">
  <title>ESP32 remote control</title>
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="stylesheet" href="index.css">
</head>

<body>
  <div class="panel">
    <h1>ESP32 remote control</h1>
    <div id="led" class="%STATE%"></div>
    <button id="toggle">Toggle</button>
  </div>
  
  <script>
/**
 * ----------------------------------------------------------------------------
 * ESP32 Remote Control with WebSocket
 * ----------------------------------------------------------------------------
 * © 2020 Stéphane Calderoni
 * ----------------------------------------------------------------------------
 */

var gateway = "ws://" + window.location.hostname + "/ws";
var websocket;

// ----------------------------------------------------------------------------
// Initialization
// ----------------------------------------------------------------------------

window.addEventListener('load', onLoad);

function onLoad(event) {
    initWebSocket();
    initButton();
}

// ----------------------------------------------------------------------------
// WebSocket handling
// ----------------------------------------------------------------------------

function initWebSocket() {
    console.log('Trying to open a WebSocket connection...');
    websocket = new WebSocket(gateway);
    websocket.onopen    = onOpen;
    websocket.onclose   = onClose;
    websocket.onmessage = onMessage;
}

function onOpen(event) {
    console.log('Connection opened');
}

function onClose(event) {
    console.log('Connection closed');
    setTimeout(initWebSocket, 2000);
}

function onMessage(event) {
    let data = JSON.parse(event.data);
    document.getElementById('led').className = data.status;
}

// ----------------------------------------------------------------------------
// Button handling
// ----------------------------------------------------------------------------

function initButton() {
    document.getElementById('toggle').addEventListener('click', onToggle);
}

function onToggle(event) {
    websocket.send(JSON.stringify({'action':'toggle'}));
}
</script>
</body>

</html>

```

##### index.css 

```
@charset "UTF-8";

/**
 * ----------------------------------------------------------------------------
 * ESP32 Remote Control with WebSocket
 * ----------------------------------------------------------------------------
 * © 2020 Stéphane Calderoni
 * ----------------------------------------------------------------------------
 */

@import url("https://fonts.googleapis.com/css2?family=Roboto&display=swap");
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

html, body {
  height: 100%;
  font-family: Roboto, sans-serif;
  font-size: 12pt;
  overflow: hidden;
}

body {
  display: grid;
  grid-template-rows: 1fr;
  align-items: center;
  justify-items: center;
}

.panel {
  display: grid;
  grid-gap: 3em;
  justify-items: center;
}

h1 {
  font-size: 1.5rem;
  text-shadow: 0 2px 4px rgba(0, 0, 0, 0.3);
}

#led {
  position: relative;
  width: 5em;
  height: 5em;
  border: 2px solid #000;
  border-radius: 2.5em;
  background-image: radial-gradient(farthest-corner at 50% 20%, #b30000 0%, #330000 100%);
  box-shadow: 0 0.5em 1em rgba(102, 0, 0, 0.3);
}
#led.on {
  background-image: radial-gradient(farthest-corner at 50% 75%, red 0%, #990000 100%);
  box-shadow: 0 1em 1.5em rgba(255, 0, 0, 0.5);
}
#led:after {
  content: '';
  position: absolute;
  top: .3em;
  left: 1em;
  width: 60%;
  height: 40%;
  border-radius: 60%;
  background-image: linear-gradient(rgba(255, 255, 255, 0.4), rgba(255, 255, 255, 0.1));
}

button {
  padding: .5em .75em;
  font-size: 1.2rem;
  color: #fff;
  text-shadow: 0 -1px 1px #000;
  border: 1px solid #000;
  border-radius: .5em;
  background-image: linear-gradient(#2e3538, #73848c);
  box-shadow: inset 0 2px 4px rgba(255, 255, 255, 0.5), 0 0.2em 0.4em rgba(0, 0, 0, 0.4);
  outline: none;
}
button:active {
  transform: translateY(2px);
}

```
#### ESP32 Remote Control Websocket INO

```
/**
 * ----------------------------------------------------------------------------
 * ESP32 Remote Control with WebSocket
 * ----------------------------------------------------------------------------
 * � 2020 St�phane Calderoni
 * ----------------------------------------------------------------------------
 */

#include <Arduino.h>
#include <SPIFFS.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>

// ----------------------------------------------------------------------------
// Definition of macros
// ----------------------------------------------------------------------------

#define LED_PIN   18
#define BTN_PIN   25
#define HTTP_PORT 80

// ----------------------------------------------------------------------------
// Definition of global constants
// ----------------------------------------------------------------------------

// Button debouncing
const uint8_t DEBOUNCE_DELAY = 10; // in milliseconds

// WiFi credentials
const char *WIFI_SSID = "Virus";
const char *WIFI_PASS = "RedEyeJedi44";

// ws
const char* PARAM_INPUT_1 = "output";
const char* PARAM_INPUT_2 = "state";

// ----------------------------------------------------------------------------
// Definition of the LED component
// ----------------------------------------------------------------------------

struct Led {
    // state variables
    uint8_t pin;
    bool    on;

    // methods
    void update() {
        digitalWrite(pin, on ? HIGH : LOW);
    }
};

// ----------------------------------------------------------------------------
// Definition of the Button component
// ----------------------------------------------------------------------------

struct Button {
    // state variables
    uint8_t  pin;
    bool     lastReading;
    uint32_t lastDebounceTime;
    uint16_t state;

    // methods determining the logical state of the button
    bool pressed()                { return state == 1; }
    bool released()               { return state == 0xffff; }
    bool held(uint16_t count = 0) { return state > 1 + count && state < 0xffff; }

    // method for reading the physical state of the button
    void read() {
        // reads the voltage on the pin connected to the button
        bool reading = digitalRead(pin);

        // if the logic level has changed since the last reading,
        // we reset the timer which counts down the necessary time
        // beyond which we can consider that the bouncing effect
        // has passed.
        if (reading != lastReading) {
            lastDebounceTime = millis();
        }

        // from the moment we're out of the bouncing phase
        // the actual status of the button can be determined
        if (millis() - lastDebounceTime > DEBOUNCE_DELAY) {
            // don't forget that the read pin is pulled-up
            bool pressed = reading == LOW;
            if (pressed) {
                     if (state  < 0xfffe) state++;
                else if (state == 0xfffe) state = 2;
            } else if (state) {
                state = state == 0xffff ? 0 : 0xffff;
            }
        }

        // finally, each new reading is saved
        lastReading = reading;
    }
};

// ----------------------------------------------------------------------------
// Definition of global variables
// ----------------------------------------------------------------------------

Led    onboard_led = { 17, false };
Led    led         = { LED_PIN, false };
Button button      = { BTN_PIN, HIGH, 0, 0 };

AsyncWebServer server(HTTP_PORT);
AsyncWebSocket ws("/ws");

// ----------------------------------------------------------------------------
// SPIFFS initialization
// ----------------------------------------------------------------------------

void initSPIFFS() {
  if (!SPIFFS.begin()) {
    Serial.println("Cannot mount SPIFFS volume...");
    while (1) {
        onboard_led.on = millis() % 200 < 50;
        onboard_led.update();
    }
  }
}

// ----------------------------------------------------------------------------
// Connecting to the WiFi network
// ----------------------------------------------------------------------------

void initWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  Serial.printf("Trying to connect [%s] ", WiFi.macAddress().c_str());
  while (WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
      delay(500);
  }
  Serial.printf(" %s\n", WiFi.localIP().toString().c_str());
}

// ----------------------------------------------------------------------------
// Web server initialization
// ----------------------------------------------------------------------------

String processor(const String &var) {
    return String(var == "STATE" && led.on ? "on" : "off");
}

void onRootRequest(AsyncWebServerRequest *request) {
  request->send(SPIFFS, "/index.html", "text/html", false, processor);
}

void initWebServer() {
    server.on("/", onRootRequest);
    server.serveStatic("/", SPIFFS, "/");

   server.on("/gpio", HTTP_GET, [] (AsyncWebServerRequest *request) {
      String inputMessage1;
      String inputMessage2;
      // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
      if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2)) {
        inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
        inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
        Serial.println(inputMessage1.toInt(), inputMessage2.toInt());
        digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
      }
      else {
        inputMessage1 = "No message sent";
        inputMessage2 = "No message sent";
      }
      Serial.print("GPIO: ");
      Serial.print(inputMessage1);
      Serial.print(" - Set to: ");
      Serial.println(inputMessage2);
      // send OK
      request->send(200, "text/plain", "OK");
      //  Send JSON styled message to the socket/Event_bus with the update
      // {inputMessage1 : inputMessage2}
      //Serial.printf("Sending to [%u]: %s\n", client_num, msg_buf);
      //webSocket.sendTXT(client_num, msg_buf);
    });
    
    server.begin();
}

// ----------------------------------------------------------------------------
// WebSocket initialization
// ----------------------------------------------------------------------------

void notifyClients() {
    const uint8_t size = JSON_OBJECT_SIZE(1);
    StaticJsonDocument<size> json;
    json["status"] = led.on ? "on" : "off";

    char data[17];
    size_t len = serializeJson(json, data);
    ws.textAll(data, len);
}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
    AwsFrameInfo *info = (AwsFrameInfo*)arg;
    if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {

        const uint8_t size = JSON_OBJECT_SIZE(1);
        StaticJsonDocument<size> json;
        DeserializationError err = deserializeJson(json, data);
        if (err) {
            Serial.print(F("deserializeJson() failed with code "));
            Serial.println(err.c_str());
            return;
        }

        const char *action = json["action"];
        if (strcmp(action, "toggle") == 0) {
            led.on = !led.on;
            notifyClients();
        }

    }
}

void onEvent(AsyncWebSocket       *server,
             AsyncWebSocketClient *client,
             AwsEventType          type,
             void                 *arg,
             uint8_t              *data,
             size_t                len) {

    switch (type) {
        case WS_EVT_CONNECT:
            Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
            break;
        case WS_EVT_DISCONNECT:
            Serial.printf("WebSocket client #%u disconnected\n", client->id());
            break;
        case WS_EVT_DATA:
            handleWebSocketMessage(arg, data, len);
            break;
        case WS_EVT_PONG:
        case WS_EVT_ERROR:
            break;
    }
}

void initWebSocket() {
    ws.onEvent(onEvent);
    server.addHandler(&ws);
}

// ----------------------------------------------------------------------------
// Initialization
// ----------------------------------------------------------------------------

void setup() {
    pinMode(onboard_led.pin, OUTPUT);
    pinMode(led.pin,         OUTPUT);
    pinMode(button.pin,      INPUT);
    pinMode(18, OUTPUT);
    digitalWrite(18, LOW);
    pinMode(19, OUTPUT);
    digitalWrite(19, LOW);
    pinMode(23, OUTPUT);
    digitalWrite(23, LOW);

    Serial.begin(115200); delay(500);

    initSPIFFS();
    initWiFi();
    initWebSocket();
    initWebServer();
}

// ----------------------------------------------------------------------------
// Main control loop
// ----------------------------------------------------------------------------

void loop() {
    //ws.cleanupClients();

    button.read();

    if (button.pressed()) {
        led.on = !led.on;
        notifyClients();
    }
    
    onboard_led.on = millis() % 1000 < 50;

    led.update();
    onboard_led.update();
}

```

## 2.5 Proof of work

< Add video and/or photos of your tests and proof it works!>

## 2.6 Files & Code

<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 2.7 References & Credits

<add all used references & give credits to those u used code and libraries from>
