# 4. Business & Marketing


###### Carribean Agro Business 

Poultry, especially chicken, are common ingredients in Caribbean cuisine and a basic source of food security for local populations. The relatively higher prices for beef and other meat products may have further contributed to the popularity and preference for chicken over other poultry such as turkeys, ducks and geese.

Worldwide, more than 50 billion chickens are reared annually as a source of food, both for their meat and eggs. Chicken farmed for meat are called broilers whilst those farmed for eggs are called egg-laying hens.

The majority of poultry are raised using intensive farming techniques. According to the Worldwatch Institute, 74% of the world’s poultry meat and 68% of eggs are produced this way. The institute also states that global production of meat has increased more than fivefold since 1950 and that factory farming is the fastest growing method of animal production worldwide.

###### Current situation

The poultry industry has much potential. According to the FAO’s summary of proceedings from the “Regional Consultation on Policy and Programmatic Actions to Address High Food Prices in the Caribbean”, with annual gross income estimated at over US $500 million, it is the largest regional agro-industrial enterprise in the region. Yearly production averages approximately 200,000 metric tonnes (MT) of meat and 29,800 MT of table eggs.

Some Caribbean countries, especially Jamaica, and Trinidad and Tobago, have domestic poultry industries that not only supply their local markets but also export small quantities to other regional markets.

According to the United States Department of Agriculture in “Livestock and Poultry: World Markets and Trade”, the US in 2010 remained the leading producer of ready-to-cook broiler meat followed by China and Brazil with production of 16,348,000, 12,550,000 and 11,420,000 MT respectively. Regarding imports, Japan and the European Union were the major importers with 2010 imports totaling 745,000 and 680,000 MT respectively.

The Caribbean region’s producers face multiple challenges that increase the cost and risk of production, placing them at a price disadvantage vis-à-vis larger, overseas competitors. There have been strong concerns that poultry imports were “crowding out” domestic production especially with frozen poultry. Producers in Jamaica, Trinidad and Tobago, Barbados, Belize and Guyana are particularly vulnerable. 

Business case

###### Why invest in poultry production?

1. Poultry is the largest agro-industrial enterprise surpassing both sugar and rice
2. Annual gross industry sale is estimated at over US $500 million
3. New investment injection is estimated at US $12 million a year (in areas such as tunnel–ventilated housing, hatcheries, processing plants, feed mills, grain reception and human resource development)
4. The industry employs over 75,000 persons directly and is the largest generator of small business and rural entrepreneurship
5. 82% of all animal protein eaten in the Caribbean is from poultry
6. Significant investment opportunities exist in feed and hatching egg production, value added products and increased intra-regional trade (accounts for only 1% of regional  production)

## 4.1 Objectives

<Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken>


## 4.3 Ananlyze your market & segments

<Identify Customer segments, value proposition, channels and identify monitizable pains>

## 4.4 Build your business canvas

<With the help or (Canvanizer Online)[https://canvanizer.com/choose/business-model-canvases] create a business canvas for your project>

## 2.5 Build your pitch deck

< 1. Build your Problem Solution Pitch for Idea Pitching!>
< 2. Build pitch deck for products/investor pitch>

## 4.6 Make a project Poster

<Build a poster for your project describing in a nutshell what it is for expo purposes>

![](https://image.slidesharecdn.com/poster-iot-virtualization-v4-160909161427/95/iot-virtualization-poster-1-638.jpg?cb=1473437776)

## 4.7 Files & Code

<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 4.8 References & Credits

<add all used references & give credits to those u used code and libraries from>
